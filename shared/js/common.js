//ボタンon,off切り替えjs
// $(function(){
// 	$('a img.btn').hover(function(){
// 	$(this).attr('src', $(this).attr('src').replace('_off', '_on'));
// 		}, function(){
// 			if (!$(this).hasClass('current_page')) {
// 			$(this).attr('src', $(this).attr('src').replace('_on', '_off'));
// 		}
// 	});
// });



/* ------------
// ------------ スマホ ナビスライド ------------
$(function(){
	$('.sp_nav_btn').click(function(){
		$('#SpNavi').slideToggle("normal");
	});
});
------------ */


/* ------------
// ------------ スムーススクロール ------------
$(function(){
	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});
 ------------ */

//
//
//  // ------------ ISEKIgooglemap1 ------------
//
//  google.maps.event.addDomListener(window, 'load', function() {
// 		 var map = document.getElementById("map_canvas1");
// 		var options = {
// 					 zoom: 14,
// 				 center: new google.maps.LatLng(43.080584, 141.366012),
// 				mapTypeId: google.maps.MapTypeId.ROADMAP
// 			};
// 		new google.maps.Map(map, options);
// 			var markerOptions = {
// 					position: LatLng,
// 					map: map,
// 			};
// 			var marker = new google.maps.Marker(markerOptions);
// });
//  // ------------ ISEKIgooglemap2 ------------
//
//  google.maps.event.addDomListener(window, 'load', function() {
// 		 var map = document.getElementById("map_canvas2");
// 		var options = {
// 					 zoom: 14,
// 				 center: new google.maps.LatLng(43.042103, 141.391360),
// 				mapTypeId: google.maps.MapTypeId.ROADMAP
// 			};
// 		new google.maps.Map(map, options);
// });
//  // ------------ ISEKIgooglemap3 ------------
//
//  google.maps.event.addDomListener(window, 'load', function() {
// 		 var map = document.getElementById("map_canvas3");
// 		var options = {
// 					 zoom: 14,
// 				 center: new google.maps.LatLng(43.080533, 141.322658),
// 				mapTypeId: google.maps.MapTypeId.ROADMAP
// 			};
// 		new google.maps.Map(map, options);
// });


// ------------ googlemap表示 MAP1------------
$(function(){
function googleMap() {

	var latlng = new google.maps.LatLng(43.080584, 141.366012);// 座標

	var myOptions = {
	zoom: 15, //拡大比率
	center: latlng,
	mapTypeControlOptions: { mapTypeIds: ['style', google.maps.MapTypeId.ROADMAP] },
	scrollwheel: false, //スクロール設定 スクロール可true スクロール不可false
	disableDefaultUI: true //コントロールの表示設定 コントロール有false コントロール無true
	};

	var map = new google.maps.Map(document.getElementById('map_canvas1'), myOptions);

	//ピンのスタイル設定
	//var icon = new google.maps.MarkerImage('/icon.png',//画像url
	//new google.maps.Size(70,84),//アイコンサイズ
	//new google.maps.Point(0,0)//アイコン位置
	//);

	var markerOptions = {
	position: latlng,
	map: map,
	//icon: icon,//ピンのスタイル
	title: '地図',//タイトル
	//animation: google.maps.Animation.DROP//アニメーション 不使用時にコメントアウト
	};

	var marker = new google.maps.Marker(markerOptions);

	//取得スタイルの貼り付け
	// var styleOptions = [
	// {
	// "stylers": [
	// { "hue": '#003366' }//カラー変更不要時にコメントアウト
	// ]
	// }
	// ];

	var styledMapOptions = { name: '色変更' }//地図右上のタイトル

	var sampleType = new google.maps.StyledMapType(styledMapOptions);
	map.mapTypes.set('style', sampleType);
	map.setMapTypeId('style');
	};

	google.maps.event.addDomListener(window, 'load', function() {
	googleMap();

});
});



// ------------ googlemap表示 MAP2------------
$(function(){
function googleMap() {

	var latlng = new google.maps.LatLng(43.042103, 141.391360);// 座標

	var myOptions = {
	zoom: 15, //拡大比率
	center: latlng,
	mapTypeControlOptions: { mapTypeIds: ['style', google.maps.MapTypeId.ROADMAP] },
	scrollwheel: false, //スクロール設定 スクロール可true スクロール不可false
	disableDefaultUI: true //コントロールの表示設定 コントロール有false コントロール無true
	};

	var map = new google.maps.Map(document.getElementById('map_canvas2'), myOptions);

	//ピンのスタイル設定
	//var icon = new google.maps.MarkerImage('/icon.png',//画像url
	//new google.maps.Size(70,84),//アイコンサイズ
	//new google.maps.Point(0,0)//アイコン位置
	//);

	var markerOptions = {
	position: latlng,
	map: map,
	//icon: icon,//ピンのスタイル
	title: '地図',//タイトル
	//animation: google.maps.Animation.DROP//アニメーション 不使用時にコメントアウト
	};

	var marker = new google.maps.Marker(markerOptions);

	//取得スタイルの貼り付け
	// var styleOptions = [
	// {
	// "stylers": [
	// { "hue": '#003366' }//カラー変更不要時にコメントアウト
	// ]
	// }
	// ];

	var styledMapOptions = { name: '色変更' }//地図右上のタイトル

	var sampleType = new google.maps.StyledMapType(styledMapOptions);
	map.mapTypes.set('style', sampleType);
	map.setMapTypeId('style');
	};

	google.maps.event.addDomListener(window, 'load', function() {
	googleMap();

});
});


// ------------ googlemap表示 MAP3------------
$(function(){
function googleMap() {

	var latlng = new google.maps.LatLng(43.080533, 141.322658);// 座標

	var myOptions = {
	zoom: 15, //拡大比率
	center: latlng,
	mapTypeControlOptions: { mapTypeIds: ['style', google.maps.MapTypeId.ROADMAP] },
	scrollwheel: false, //スクロール設定 スクロール可true スクロール不可false
	disableDefaultUI: true //コントロールの表示設定 コントロール有false コントロール無true
	};

	var map = new google.maps.Map(document.getElementById('map_canvas3'), myOptions);

	//ピンのスタイル設定
	//var icon = new google.maps.MarkerImage('/icon.png',//画像url
	//new google.maps.Size(70,84),//アイコンサイズ
	//new google.maps.Point(0,0)//アイコン位置
	//);

	var markerOptions = {
	position: latlng,
	map: map,
	//icon: icon,//ピンのスタイル
	title: '地図',//タイトル
	//animation: google.maps.Animation.DROP//アニメーション 不使用時にコメントアウト
	};

	var marker = new google.maps.Marker(markerOptions);

	//取得スタイルの貼り付け
	// var styleOptions = [
	// {
	// "stylers": [
	// { "hue": '#003366' }//カラー変更不要時にコメントアウト
	// ]
	// }
	// ];

	var styledMapOptions = { name: '色変更' }//地図右上のタイトル

	var sampleType = new google.maps.StyledMapType(styledMapOptions);
	map.mapTypes.set('style', sampleType);
	map.setMapTypeId('style');
	};

	google.maps.event.addDomListener(window, 'load', function() {
	googleMap();

});
});
